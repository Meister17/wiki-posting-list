#ifndef BITWRITER_H
#define BITWRITER_H

#include <deque>
#include <stack>
#include <string>
#include <vector>

class BitWriter {
 public:
  BitWriter() : bits_() {
  }

  void PutBit(unsigned char bit) {
    bits_.push_back(bit);
  }

  std::vector<unsigned char> ToArray() {
    InsertLength(bits_.size());
    bits_.resize(bits_.size() + (8 - bits_.size() % 8) % 8);
    std::vector<unsigned char> bytes;
    while (!bits_.empty()) {
      unsigned char byte = 0;
      for (size_t index = 0; index < 8; ++index) {
        byte = (byte << 1) | bits_.front();
        bits_.pop_front();
      }
      bytes.push_back(byte);
    }
    return bytes;
  }

  void InsertNumber(int number) {
    std::stack<unsigned char> bits_stack;
    for (int index = 0; index < 32; ++index) {
      bits_stack.push(number & 1);
      number >>= 1;
    }
    while (!bits_stack.empty()) {
      bits_.push_back(bits_stack.top());
      bits_stack.pop();
    }
  }

 private:
  void InsertLength(int length) {
    for (int index = 0; index < 32; ++index) {
      bits_.push_front(length & 1);
      length >>= 1;
    }
  }

  std::deque<unsigned char> bits_;
};

#endif /* BITWRITER_H */