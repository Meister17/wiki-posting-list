#ifndef BITREADER_H
#define BITREADER_H

#include <queue>
#include <stack>
#include <vector>

class BitReader {
 public:
  BitReader(const std::vector<unsigned char>& bytes, int length) {
    for (size_t index = 0; length > 0 && index < bytes.size(); ++index) {
      unsigned char byte = bytes[index];
      std::stack<unsigned char> bits_stack;
      for (int i = 0; i < 8; ++i) {
        bits_stack.push(byte & 1);
        byte >>= 1;
      }
      while (!bits_stack.empty() && length > 0) {
        bits_.push(bits_stack.top());
        bits_stack.pop();
        --length;
      }
    }
  }

  bool HasLeft() const {
    return !bits_.empty();
  }

  unsigned char GetBit() {
    unsigned char bit = bits_.front();
    bits_.pop();
    return bit;
  }

 private:
  std::queue<unsigned char> bits_;
};

#endif /* BITREADER_H */