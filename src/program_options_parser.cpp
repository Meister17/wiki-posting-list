#include "./program_options_parser.h"
#include <boost/program_options.hpp>
#include <fstream>
#include <iostream>
#include <string>

using namespace boost::program_options;
using std::cout;
using std::endl;
using std::ifstream;
using std::string;

void ProgramOptionsParser::Parse(int argc, char** argv) {
  cout << "Parsing program options" << endl;
  variables_map program_options_storage;
  options_description program_options_description("Options for program");
  program_options_description.add_options()
      ("configuration_filename,c", value<string>(&configuration_filename_),
          "Name of configuration file")
      ("input_directory_name,i", value<string>(&input_directory_name_),
          "Name of input directory for processing")
      ("vocabulary_filename,v", value<string>(&vocabulary_filename_),
          "Name of file for printing vocabulary")
      ("document_names_filename,d", value<string>(&document_names_filename_),
          "Name of file for printing document names")
      ("posting_list_filename,p", value<string>(&posting_list_filename_),
          "Name of file for printing posting list")
      ("compression_method,m", value<string>(&compression_method_),
          "Name of compression method to use")
      ("mode,r", value<string>(&mode_), "Program mode")
      ("exact_duplicates_filename,e",
          value<string>(&exact_duplicates_filename_),
          "Name of file for printing found exact duplicates")
      ("near_duplicates_filename,n", value<string>(&near_duplicates_filename_),
          "Name of file for printing found near duplicates")
      ("exact_duplicates_algorithm",
          value<string>(&exact_duplicates_algorithm_),
          "Algorithm for finding exact duplicates")
      ("near_duplicates_algorithm", value<string>(&near_duplicates_algorithm_),
          "Algorithm for finding near duplicates")
      ("top_results_number,t", value<size_t>(&top_results_number_),
          "Maximum number of search results");
  store(parse_command_line(argc, argv, program_options_description),
        program_options_storage);
  notify(program_options_storage);
  ifstream file(configuration_filename_.data());
  if (file) {
    file.close();
    store(parse_config_file<char>(configuration_filename_.data(),
                                  program_options_description),
          program_options_storage);
    notify(program_options_storage);
  }
}