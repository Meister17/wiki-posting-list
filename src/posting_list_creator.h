#ifndef POSTING_LIST_CREATOR_H
#define POSTING_LIST_CREATOR_H

#include "compressors/compressor.h"
#include <cstdio>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

class PostingListCreator {
 public:
  void DoJob(const std::string& input_directory_name,
             const std::string& vocabulary_filename,
             const std::string& document_names_filename,
             const std::string& posting_list_filename,
             const std::string& compression_method,
             const std::string& exact_duplicates_filename,
             const std::string& exact_duplicates_algorithm,
             const std::string& near_duplicates_filename,
             const std::string& near_duplicates_algorithm) const;
 private:
  void CreatePostingList(const std::string& input_directory_name,
                         const std::string& vocabulary_filename,
                         const std::string& document_names_filename,
                         const std::string& posting_list_filename,
                         const std::string& compression_method) const;
  void FindExactDuplicates(const std::string& input_directory_name,
                           const std::string& exact_duplicates_algorithm,
                           const std::string& exact_duplicates_filename) const;
  void FindNearDuplicates(const std::string& input_directory_name,
                          const std::string& near_duplicates_algorithm,
                          const std::string& near_duplicates_filename) const;
  void PrintDuplicates(
      const std::string& filename,
      const std::vector<std::unordered_set<std::string> >& duplicates) const;
  void ParseFile(const std::string& filename,
                 const std::string& doc_name,
                 std::vector<std::string>* document_names,
                 std::unordered_map<std::string, int>* vocabulary,
                 std::vector<std::vector<int> >* posting_lists) const ;
  std::string ExtractWord(FILE* file, int& read_symbol) const;
  void PrintDocumentNames(const std::string& filename,
                          const std::vector<std::string>& document_names) const;
  void PrintVocabulary(const std::string& filename,
                       const std::unordered_map<std::string, int>& vocabulary)
                       const;
  void PrintPostingLists(
      const std::string& filename,
      std::vector<std::vector<int> >* posting_lists,
      PostingListCompressor* posting_list_compressor) const;
};

#endif /* POSTING_LIST_CREATOR_H */