#ifndef WIKI_SEARCHER_H
#define WIKI_SEARCHER_H

#include "decompressors/decompressor.h"
#include <string>
#include <unordered_map>
#include <vector>

class WikiSearcher {
 public:
  explicit WikiSearcher(size_t top_results_number)
      : TOP_RESULTS_NUMBER_(top_results_number),
        posting_lists_(),
        vocabulary_(),
        document_names_() {
  }

  void Initialize(const std::string& posting_list_filename,
                  const std::string& vocabulary_filename,
                  const std::string& document_names,
                  const std::string& compression_method);

  void SearchWiki() const;

 private:
  void ParseVocabulary(const std::string& filename);
  void ParseDocumentNames(const std::string& filename);
  void ParsePostingLists(const std::string& filename,
                         PostingListDecompressor* posting_list_decompressor);
  bool ReadQuery(std::string* query) const;
  std::vector<int> SearchWikiForQuery(const std::string& query) const;
  void PrintSearchResults(const std::vector<int>& results) const;

  const size_t TOP_RESULTS_NUMBER_;
  std::vector<std::vector<int> > posting_lists_;
  std::unordered_map<std::string, int> vocabulary_;
  std::vector<std::string> document_names_;
};

#endif /* WIKI_SEARCHER_H */