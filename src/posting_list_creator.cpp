#include "./posting_list_creator.h"
#include "compressors/compressor.h"
#include "compressors/elias_delta_compressor.h"
#include "compressors/elias_gamma_compressor.h"
#include "compressors/elias_omega_compressor.h"
#include "compressors/exp_golomb_compressor.h"
#include "compressors/levenshtein_compressor.h"
#include "compressors/no_posting_list_compressor.h"
#include "compressors/vbyte_compressor.h"
#include "duplicates/duplicates_finder.h"
#include "duplicates/exact_duplicates_finder.h"
#include "duplicates/imatch.h"
#include <algorithm>
#include <boost/filesystem.hpp>
#include <boost/threadpool.hpp>
#include <cctype>
#include <cstdio>
#include <memory>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace boost::filesystem;
using boost::threadpool::pool;
using std::make_pair;
using std::runtime_error;
using std::sort;
using std::string;
using std::unique_ptr;
using std::unordered_map;
using std::unordered_set;
using std::vector;

void PostingListCreator::DoJob(
    const string& input_directory_name,
    const string& vocabulary_filename,
    const string& document_names_filename,
    const string& posting_list_filename,
    const string& compression_method,
    const string& exact_duplicates_filename,
    const string& near_duplicates_filename,
    const string& exact_duplicates_algorithm,
    const string& near_duplicates_algorithm) const {
  pool thread_pool(3);
  thread_pool.schedule(boost::bind(&PostingListCreator::CreatePostingList,
                                   this,
                                   input_directory_name,
                                   vocabulary_filename,
                                   document_names_filename,
                                   posting_list_filename,
                                   compression_method));
  if (!exact_duplicates_algorithm.empty() &&
      !exact_duplicates_filename.empty()) {
    thread_pool.schedule(boost::bind(&PostingListCreator::FindExactDuplicates,
                                     this,
                                     input_directory_name,
                                     exact_duplicates_algorithm,
                                     exact_duplicates_filename));
  }
  if (!near_duplicates_algorithm.empty() &&
      !near_duplicates_filename.empty()) {
    thread_pool.schedule(boost::bind(&PostingListCreator::FindNearDuplicates,
                                     this,
                                     input_directory_name,
                                     near_duplicates_algorithm,
                                     near_duplicates_filename));
  }
}

void PostingListCreator::CreatePostingList(
    const string& input_directory_name,
    const string& vocabulary_filename,
    const string& document_names_filename,
    const string& posting_list_filename,
    const string& compression_method) const {
  printf("Creating posting list\n");
  unique_ptr<PostingListCompressor> posting_list_compressor;
  if (compression_method == "no") {
    posting_list_compressor.reset(new NoPostingListCompressor());
  } else if (compression_method == "elias_gamma") {
    posting_list_compressor.reset(new EliasGammaCompressor());
  } else if (compression_method == "elias_delta") {
    posting_list_compressor.reset(new EliasDeltaCompressor());
  } else if (compression_method == "elias_omega") {
    posting_list_compressor.reset(new EliasOmegaCompressor());
  } else if (compression_method == "exp_golomb") {
    posting_list_compressor.reset(new ExpGolombCompressor());
  } else if (compression_method == "levenshtein") {
    posting_list_compressor.reset(new LevenshteinCompressor());
  } else if (compression_method == "vbyte") {
    posting_list_compressor.reset(new VByteCompressor());
  } else {
    throw runtime_error("Selected compression method is not supported");
  }
  vector<string> document_names;
  unordered_map<string, int> vocabulary;
  vector<vector<int> > posting_lists;
  int number_file = 0;
  for (directory_iterator iterator(input_directory_name);
       iterator != directory_iterator(); ++iterator) {
    ++number_file;
    if (number_file % 10000 == 0) {
      printf("Processed %d files\n", number_file);
    }
    ParseFile(iterator->path().string(),
              iterator->path().filename().string(),
              &document_names,
              &vocabulary,
              &posting_lists);
  }
  PrintVocabulary(vocabulary_filename, vocabulary);
  PrintDocumentNames(document_names_filename, document_names);
  PrintPostingLists(posting_list_filename,
                    &posting_lists,
                    posting_list_compressor.get());
}

void PostingListCreator::FindExactDuplicates(
    const string& input_directory_name,
    const string& exact_duplicates_algorithm,
    const string& exact_duplicates_filename) const {
  printf("Start finding exact duplicates\n");
  unique_ptr<DuplicatesFinder> exact_duplicates_finder;
  if (exact_duplicates_algorithm == "md5") {
    exact_duplicates_finder.reset(new ExactDuplicatesFinder());
  } else {
    throw runtime_error("Selected exact duplicates finder is not supported");
  }
  vector<unordered_set<string> > duplicates =
      exact_duplicates_finder->FindDuplicates(input_directory_name);
  PrintDuplicates(exact_duplicates_filename, duplicates);
}

void PostingListCreator::FindNearDuplicates(
    const string& input_directory_name,
    const string& near_duplicates_algorithm,
    const string& near_duplicates_filename) const {
  printf("Start finding near duplicates\n");
  unique_ptr<DuplicatesFinder> near_duplicates_finder;
  if (near_duplicates_algorithm == "imatch") {
    near_duplicates_finder.reset(new IMatch());
  } else {
    throw runtime_error("Selected near duplicates finder is not supported");
  }
  vector<unordered_set<string> > duplicates =
      near_duplicates_finder->FindDuplicates(input_directory_name);
  PrintDuplicates(near_duplicates_filename, duplicates);
}

void PostingListCreator::PrintDuplicates(
    const string& filename,
    const vector<unordered_set<string> >& duplicates) const {
  printf("Printing duplicates\n");
  FILE* file = fopen(filename.data(), "w");
  if (!file) {
    throw runtime_error("Failed to open file for printing duplicates");
  }
  for (auto& duplicate: duplicates) {
    for (auto& element: duplicate) {
      fprintf(file, "%s\t", element.data());
    }
    if (!duplicate.empty()) {
      fprintf(file, "\n");
    }
  }
  fclose(file);
}

void PostingListCreator::ParseFile(const string& filename,
                                   const string& doc_name,
                                   vector<string>* document_names,
                                   unordered_map<string, int>* vocabulary,
                                   vector<vector<int> >* posting_lists) const {
  FILE* file = fopen(filename.data(), "r");
  if (!file) {
    throw runtime_error("Failed to open file for reading");
  };
  document_names->push_back(doc_name);
  int doc_id = document_names->size();
  unordered_set<string> words;
  int read_symbol;
  while ((read_symbol = fgetc(file)) != EOF) {
    string word = ExtractWord(file, read_symbol);
    if (!word.empty()) {
      words.insert(word);
    }
  }
  for (auto& word: words) {
    auto result = vocabulary->insert(make_pair(word, vocabulary->size()));
    if (result.second) {
      posting_lists->push_back(vector<int>());
    }
    int position = result.first->second;
    posting_lists->at(position).push_back(doc_id);
  }
  fclose(file);
}

string PostingListCreator::ExtractWord(FILE* file, int& read_symbol) const {
  while (read_symbol != EOF && isspace(read_symbol)) {
    read_symbol = fgetc(file);
  }
  string word;
  bool is_word = true;
  while (read_symbol != EOF && !isspace(read_symbol)) {
    if (isalpha(read_symbol)) {
      word.push_back(read_symbol);
    } else {
      is_word = false;
    }
    read_symbol = fgetc(file);
  }
  if (is_word) {
    return word;
  } else {
    return "";
  }
}

void PostingListCreator::PrintVocabulary(
    const string& filename,
    const unordered_map<string, int>& vocabulary) const {
  printf("Printing vocabulary\n");
  FILE* file = fopen(filename.data(), "w");
  if (!file) {
    throw runtime_error("Failed to open file for printing vocabulary");
  }
  vector<string> dictionary(vocabulary.size());
  for (auto& word: vocabulary) {
    dictionary[word.second] = word.first;
  }
  for (auto& element: dictionary) {
    fprintf(file, "%s\n", element.data());
  }
  fclose(file);
}

void PostingListCreator::PrintDocumentNames(
    const string& filename,
    const vector<string>& document_names) const {
  printf("Printing document names\n");
  FILE* file = fopen(filename.data(), "w");
  if (!file) {
    throw runtime_error("Failed to open file for printing document names");
  }
  for (auto& doc_name: document_names) {
    fprintf(file, "%s\n", doc_name.data());
  }
  fclose(file);
}

void PostingListCreator::PrintPostingLists(
    const string& filename,
    vector<vector<int> >* posting_lists,
    PostingListCompressor* posting_list_compressor) const {
  printf("Printing posting list\n");
  FILE* file = fopen(filename.data(), "wb");
  if (!file) {
    throw runtime_error("Failed to open file for printing posting list");
  }
  for (auto& element: *posting_lists) {
    sort(element.begin(), element.end());
    vector<unsigned char> compressed_list =
        posting_list_compressor->CompressPostingList(element);
    if (fwrite(compressed_list.data(), 1, compressed_list.size(), file) !=
        compressed_list.size()) {
      fclose(file);
      throw runtime_error("Failed to print posting lists");
    }
  }
  fclose(file);
}