#ifndef IMATCH_H
#define IMATCH_H

#include "duplicates_finder.h"
#include <cstdio>
#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

class IMatch : public DuplicatesFinder {
 public:
  IMatch()
      : SEED_NUMBER_(239),
        NUMBER_OF_REPEATS_(10),
        REMOVING_PART_(0.3),
        LOWER_THRESHOLD_(0.2),
        UPPER_THRESHOLD_(0.8) {
  }

  std::vector<std::unordered_set<std::string> > FindDuplicates(
      const std::string& input_directory_name) const;

 private:
  void GetLexiconsWithNIDF(const std::string& input_directory_name,
                           std::unordered_map<std::string, double>* lexicon,
                           std::vector<std::set<std::string> >* dicts,
                           std::vector<std::string>* document_names) const;
  std::string ExtractWord(FILE* file, int& read_symbol) const;
  void IntersectDictsWithVocabulary(
      const std::unordered_set<std::string>& vocabulary,
      std::vector<std::set<std::string> >* dicts) const;
  void FindDuplicatesWithDict(
      const std::vector<std::set<std::string> >& dicts,
      const std::vector<std::string>& document_names,
      const std::unordered_set<std::string>& vocabulary,
      std::vector<std::unordered_set<std::string> >* duplicates,
      std::unordered_map<std::string, int>* duplicates_indices) const;
  std::unordered_set<std::string> RemoveSomeWords(
      const std::unordered_set<std::string>& vocabulary) const;

  const int SEED_NUMBER_;
  const int NUMBER_OF_REPEATS_;
  const double REMOVING_PART_;
  const double LOWER_THRESHOLD_;
  const double UPPER_THRESHOLD_;
};

#endif /* IMATCH_H */