#ifndef DUPLICATES_FINDER_H
#define DUPLICATES_FINDER_H

#include <string>
#include <unordered_set>
#include <vector>

class DuplicatesFinder {
 public:
  virtual std::vector<std::unordered_set<std::string> > FindDuplicates(
      const std::string& input_directory_name) const = 0;

  virtual ~DuplicatesFinder() {
  }
};

#endif /* DUPLICATES_FINDER_H */