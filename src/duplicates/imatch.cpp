#include "imatch.h"
#include <boost/filesystem.hpp>
#include <hashlib++/hashlibpp.h>
#include <algorithm>
#include <cmath>
#include <cctype>
#include <cstdlib>
#include <limits>
#include <set>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace boost::filesystem;
using std::make_pair;
using std::max;
using std::min;
using std::numeric_limits;
using std::runtime_error;
using std::set;
using std::string;
using std::unordered_map;
using std::unordered_set;
using std::vector;

vector<unordered_set<string> > IMatch::FindDuplicates(
    const string& input_directory_name) const {
  vector<unordered_set<string> > duplicates;
  unordered_map<string, double> lexicon;
  vector<set<string> > dicts;
  vector<string> document_names;
  GetLexiconsWithNIDF(input_directory_name, &lexicon, &dicts, &document_names);
  unordered_set<string> vocabulary;
  for (auto& element: lexicon) {
    if (element.second >= LOWER_THRESHOLD_ &&
        element.second <= UPPER_THRESHOLD_) {
      vocabulary.insert(element.first);
    }
  }
  IntersectDictsWithVocabulary(vocabulary, &dicts);
  unordered_map<string, int> duplicates_indices;
  FindDuplicatesWithDict(dicts,
                         document_names,
                         vocabulary,
                         &duplicates,
                         &duplicates_indices);
  srand(SEED_NUMBER_);
  for (int number = 0; number < NUMBER_OF_REPEATS_; ++number) {
    unordered_set<string> new_vocabulary = RemoveSomeWords(vocabulary);
    FindDuplicatesWithDict(dicts,
                           document_names,
                           new_vocabulary,
                           &duplicates,
                           &duplicates_indices);
  }
  return duplicates;
}

void IMatch::GetLexiconsWithNIDF(const string& input_directory_name,
                                 unordered_map<string, double>* lexicon,
                                 vector<set<string> >* dicts,
                                 vector<string>* document_names) const {
  for (directory_iterator iterator(input_directory_name); iterator !=
       directory_iterator(); ++iterator) {
    document_names->push_back(iterator->path().filename().string());
    FILE* file = fopen(iterator->path().string().data(), "r");
    if (!file) {
      throw runtime_error("Failed to open file for reading");
    }
    dicts->push_back(set<string>());
    int read_symbol;
    while ((read_symbol = fgetc(file)) != EOF) {
      string word = ExtractWord(file, read_symbol);
      if (!word.empty()) {
        dicts->back().insert(word);
      }
    }
    for (auto& element: dicts->back()) {
      auto result = lexicon->insert(make_pair(element, 1.0));
      if (!result.second) {
        ++(result.first->second);
      }
    }
    fclose(file);
  }
  double min_idf = numeric_limits<double>::max();
  double max_idf = numeric_limits<double>::min();
  for (auto& element: *lexicon) {
    element.second = log((double)document_names->size()) -
      log(element.second);
    min_idf = min(min_idf, element.second);
    max_idf = max(max_idf, element.second);
  }
  for (auto& element: *lexicon) {
    element.second = (element.second - min_idf) / (max_idf - min_idf);
  }
}

string IMatch::ExtractWord(FILE* file, int& read_symbol) const {
  while (read_symbol != EOF && isspace(read_symbol)) {
    read_symbol = fgetc(file);
  }
  string word;
  bool is_word = true;
  while (read_symbol != EOF && !isspace(read_symbol)) {
    if (isalpha(read_symbol)) {
      word.push_back(read_symbol);
    } else {
      is_word = false;
    }
    read_symbol = fgetc(file);
  }
  if (is_word) {
    return word;
  } else {
    return "";
  }
}

void IMatch::IntersectDictsWithVocabulary(
    const unordered_set<string>& vocabulary,
    vector<set<string> >* dicts) const {
  for (auto& dict: *dicts) {
    for (set<string>::iterator iterator = dict.begin();
         iterator != dict.end();) {
      if (vocabulary.find(*iterator) == vocabulary.end()) {
        iterator = dict.erase(iterator);
      } else {
        ++iterator;
      }
    }
  }
}

void IMatch::FindDuplicatesWithDict(
    const vector<set<string> >& dicts,
    const vector<string>& document_names,
    const unordered_set<string>& vocabulary,
    vector<unordered_set<string> >* duplicates,
    unordered_map<string, int>* duplicates_indices) const {
  hashwrapper* sha1counter = new sha1wrapper();
  unordered_map<string, vector<int> > hash_map;
  for (size_t index = 0; index < dicts.size(); ++index) {
    string text = "";
    for (auto& element: dicts[index]) {
      if (vocabulary.find(element) != vocabulary.end()) {
        text.append(element + " ");
      }
    }
    string hash = sha1counter->getHashFromString(text);
    auto result = hash_map.insert(make_pair(hash, vector<int>()));
    if (!result.second) {
      result.first->second.push_back(index);
    }
  }
  for (auto& hash_element: hash_map) {
    if (hash_element.second.size() > 1) {
      unordered_set<string> new_duplicates;
      vector<int> join_duplicates;
      size_t head_index = duplicates->size();
      for (auto& element: hash_element.second) {
        new_duplicates.insert(document_names[element]);
        auto find_iterator = duplicates_indices->find(document_names[element]);
        if (find_iterator != duplicates_indices->end()) {
          join_duplicates.push_back(find_iterator->second);
          head_index = min(head_index, (size_t)find_iterator->second);
        }
      }
      if (join_duplicates.empty()) {
        duplicates->push_back(unordered_set<string>());
      } else if (join_duplicates.size() > 1) {
        for (size_t index = 0; index < join_duplicates.size(); ++index) {
          if (index != head_index) {
            for (auto& duplicate: duplicates->at(index)) {
              duplicates->at(head_index).insert(duplicate);
              duplicates_indices->at(duplicate) = head_index;
            }
            duplicates->at(index).clear();
          }
        }
      }
      for (auto& duplicate: new_duplicates) {
        duplicates->at(head_index).insert(duplicate);
        auto result = duplicates_indices->insert(make_pair(duplicate,
                                                           head_index));
        if (!result.second) {
          result.first->second = head_index;
        }
      }
    }
  }
  delete sha1counter;
}

unordered_set<string> IMatch::RemoveSomeWords(
    const unordered_set<string>& vocabulary) const {
  vector<string> temp_vocabulary;
  for (auto& word: vocabulary) {
    temp_vocabulary.push_back(word);
  }
  random_shuffle(temp_vocabulary.begin(), temp_vocabulary.end());
  int new_size = (int)((1.0 - REMOVING_PART_) * temp_vocabulary.size());
  unordered_set<string> new_vocabulary;
  for (int index = 0; index < new_size; ++index) {
    new_vocabulary.insert(temp_vocabulary[index]);
  }
  return new_vocabulary;
}