#ifndef EXACT_DUPLICATES_FINDER_H
#define EXACT_DUPLICATES_FINDER_H

#include "duplicates_finder.h"
#include <cstdio>
#include <string>
#include <unordered_set>
#include <vector>

class ExactDuplicatesFinder : public DuplicatesFinder {
 public:
  std::vector<std::unordered_set<std::string> > FindDuplicates(
      const std::string& input_directory_name) const;

 private:
  std::string ExtractTextFromFile(const std::string& filename) const;
  std::string ExtractWord(FILE* file, int& read_symbol) const;
};

#endif /* EXACT_DUPLICATES_FINDER_H */