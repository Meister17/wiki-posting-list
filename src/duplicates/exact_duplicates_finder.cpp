#include "exact_duplicates_finder.h"
#include <boost/filesystem.hpp>
#include <cctype>
#include <cstdio>
#include <hashlib++/hashlibpp.h>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace boost::filesystem;
using std::make_pair;
using std::runtime_error;
using std::string;
using std::unordered_map;
using std::unordered_set;
using std::vector;

vector<unordered_set<string> > ExactDuplicatesFinder::FindDuplicates(
    const string& input_directory_name) const {
  vector<unordered_set<string> > duplicates;
  hashwrapper* md5counter = new md5wrapper();
  unordered_map<string, vector<string> > hash_map;
  for (directory_iterator iterator(input_directory_name);
       iterator != directory_iterator(); ++iterator) {
    string text = ExtractTextFromFile(iterator->path().string());
    string hash = md5counter->getHashFromString(text);
    auto result = hash_map.insert(make_pair(hash, vector<string>()));
    result.first->second.push_back(iterator->path().filename().string());
  }
  for (auto& element: hash_map) {
    if (element.second.size() > 1) {
      duplicates.push_back(unordered_set<string>());
      for (auto& duplicate: element.second) {
        duplicates.back().insert(duplicate);
      }
    }
  }
  delete md5counter;
  return duplicates;
}

string ExactDuplicatesFinder::ExtractTextFromFile(const string& filename)
    const {
  string text;
  FILE* file = fopen(filename.data(), "r");
  if (!file) {
    throw runtime_error("Failed to extract text from file");
  }
  int read_symbol;
  while ((read_symbol = fgetc(file)) != EOF) {
    string word = ExtractWord(file, read_symbol);
    if (!word.empty()) {
      text.append(word);
    }
  }
  fclose(file);
  return text;
}

string ExactDuplicatesFinder::ExtractWord(FILE* file, int& read_symbol) const {
  while (read_symbol != EOF && isspace(read_symbol)) {
    read_symbol = fgetc(file);
  }
  string word;
  while (read_symbol != EOF && !isspace(read_symbol)) {
    word.push_back(read_symbol);
    read_symbol = fgetc(file);
  }
  return word;
}