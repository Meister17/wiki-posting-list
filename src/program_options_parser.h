#ifndef PROGRAM_OPTIONS_PARSER_H
#define PROGRAM_OPTIONS_PARSER_H

#include <string>

class ProgramOptionsParser {
 public:
  ProgramOptionsParser() : configuration_filename_(""),
                           input_directory_name_(""),
                           vocabulary_filename_(""),
                           document_names_filename_(""),
                           posting_list_filename_(""),
                           compression_method_(""),
                           mode_(""),
                           exact_duplicates_filename_(""),
                           near_duplicates_filename_(""),
                           exact_duplicates_algorithm_(""),
                           near_duplicates_algorithm_(""),
                           top_results_number_(100) {
  }

  void Parse(int argc, char** argv);

  std::string input_directory_name() const {
    return input_directory_name_;
  }

  std::string vocabulary_filename() const {
    return vocabulary_filename_;
  }

  std::string document_names_filename() const {
    return document_names_filename_;
  }

  std::string posting_list_filename() const {
    return posting_list_filename_;
  }

  std::string compression_method() const {
    return compression_method_;
  }

  std::string mode() const {
    return mode_;
  }

  std::string exact_duplicates_filename() const {
    return exact_duplicates_filename_;
  }

  std::string near_duplicates_filename() const {
    return near_duplicates_filename_;
  }

  std::string exact_duplicates_algorithm() const {
    return exact_duplicates_algorithm_;
  }

  std::string near_duplicates_algorithm() const {
    return near_duplicates_algorithm_;
  }

  size_t top_results_number() const {
    return top_results_number_;
  }

 private:
  std::string configuration_filename_;
  std::string input_directory_name_;
  std::string vocabulary_filename_;
  std::string document_names_filename_;
  std::string posting_list_filename_;
  std::string compression_method_;
  std::string mode_;
  std::string exact_duplicates_filename_;
  std::string near_duplicates_filename_;
  std::string exact_duplicates_algorithm_;
  std::string near_duplicates_algorithm_;
  size_t top_results_number_;
};

#endif /* PROGRAM_OPTIONS_PARSER_H */