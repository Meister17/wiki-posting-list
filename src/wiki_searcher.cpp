#include "wiki_searcher.h"
#include "decompressors/decompressor.h"
#include "decompressors/elias_delta_decompressor.h"
#include "decompressors/elias_gamma_decompressor.h"
#include "decompressors/elias_omega_decompressor.h"
#include "decompressors/exp_golomb_decompressor.h"
#include "decompressors/levenshtein_decompressor.h"
#include "decompressors/no_posting_list_decompressor.h"
#include "decompressors/vbyte_decompressor.h"
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <cctype>
#include <cmath>
#include <memory>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>

using boost::algorithm::split;
using boost::is_any_of;
using boost::token_compress_on;
using std::back_inserter;
using std::min;
using std::runtime_error;
using std::set_intersection;
using std::string;
using std::unique_ptr;
using std::unordered_map;
using std::vector;

void WikiSearcher::Initialize(const string& posting_list_filename,
                              const string& vocabulary_filename,
                              const string& document_names_filename,
                              const string& compression_method) {
  printf("Initializing wiki searcher\n");
  ParseVocabulary(vocabulary_filename);
  ParseDocumentNames(document_names_filename);
  unique_ptr<PostingListDecompressor> posting_list_decompressor;
  if (compression_method == "no") {
    posting_list_decompressor.reset(new NoPostingListDecompressor());
  } else if (compression_method == "elias_gamma") {
    posting_list_decompressor.reset(new EliasGammaDecompressor());
  } else if (compression_method == "elias_delta") {
    posting_list_decompressor.reset(new EliasDeltaDecompressor());
  } else if (compression_method == "elias_omega") {
    posting_list_decompressor.reset(new EliasOmegaDecompressor());
  } else if (compression_method == "exp_golomb") {
    posting_list_decompressor.reset(new ExpGolombDecompressor());
  } else if (compression_method == "levenshtein") {
    posting_list_decompressor.reset(new LevenshteinDecompressor());
  } else if (compression_method == "vbyte") {
    posting_list_decompressor.reset(new VByteDecompressor());
  } else {
    throw runtime_error("Selected compression method is not supported");
  }
  ParsePostingLists(posting_list_filename, posting_list_decompressor.get());
}

void WikiSearcher::SearchWiki() const {
  bool good_query = true;
  while (good_query) {
    printf("Enter the query (or hit Ctrl+D to exit the program):");
    string query;
    good_query = ReadQuery(&query);
    if (good_query) {
      vector<int> search_results = SearchWikiForQuery(query);
      PrintSearchResults(search_results);
    }
  }
  printf("\n");
}

void WikiSearcher::ParseVocabulary(const string& filename) {
  FILE* file = fopen(filename.data(), "r");
  if (!file) {
    throw runtime_error("Failed to read file with vocabulary");
  }
  int read_symbol;
  int index = 0;
  while ((read_symbol = fgetc(file)) != EOF) {
    string word;
    while (read_symbol != '\n') {
      word.push_back(read_symbol);
      read_symbol = fgetc(file);
    }
    vocabulary_[word] = index;
    ++index;
  }
  fclose(file);
}

void WikiSearcher::ParseDocumentNames(const string& filename) {
  FILE* file = fopen(filename.data(), "r");
  if (!file) {
    throw runtime_error("Failed to read file with document names");
  }
  int read_symbol;
  while ((read_symbol = fgetc(file)) != EOF) {
    string document_name;
    while (read_symbol != '\n') {
      document_name.push_back(read_symbol);
      read_symbol = fgetc(file);
    }
    document_names_.push_back(document_name);
  }
  fclose(file);
}

void WikiSearcher::ParsePostingLists(
    const string& filename,
    PostingListDecompressor* posting_list_decompressor) {
  FILE* file = fopen(filename.data(), "rb");
  if (!file) {
    throw runtime_error("Failed to read posting lists");
  }
  int read_symbol;
  while ((read_symbol = fgetc(file)) != EOF) {
    if (ungetc(read_symbol, file) == EOF) {
      fclose(file);
      throw runtime_error("Failed to start reading posting list");
    }
    unsigned char length_bytes[4];
    if (fread(length_bytes, 1, 4, file) != 4) {
      fclose(file);
      throw runtime_error("Failed to get length when reading posting lists");
    }
    unsigned int length = 0x0;
    for (size_t index = 0; index < 4; ++index) {
      length = (length << 8) | length_bytes[index];
    }
    int bytes_number = ceil(length / 8.0);
    vector<unsigned char> bytes(bytes_number);
    if (fread(bytes.data(), 1, bytes_number, file) != (size_t)bytes_number) {
      fclose(file);
      throw runtime_error("Failed to read posting list");
    }
    posting_lists_.push_back(posting_list_decompressor->DecompressPostingList(
        bytes, length));
  }
  fclose(file);
  if (posting_lists_.size() != vocabulary_.size()) {
    throw runtime_error("Wrong vocabulary and/or posting lists");
  }
}

bool WikiSearcher::ReadQuery(string* query) const {
  int read_symbol = getchar();
  while (read_symbol != EOF && read_symbol != '\n') {
    query->push_back(read_symbol);
    read_symbol = getchar();
  }
  return read_symbol != EOF;
}

vector<int> WikiSearcher::SearchWikiForQuery(const string& query) const {
  vector<string> words;
  split(words, query, is_any_of(" "), token_compress_on);
  for (auto& word: words) {
    for (auto& letter: word) {
      letter = tolower(letter);
    }
  }
  vector<int> results;
  for (auto& word: words) {
    auto iterator = vocabulary_.find(word);
    if (iterator != vocabulary_.end()) {
      if (results.empty()) {
        results = posting_lists_[iterator->second];
      } else {
        vector<int> temp_results;
        set_intersection(results.begin(),
                         results.end(),
                         posting_lists_[iterator->second].begin(),
                         posting_lists_[iterator->second].end(),
                         back_inserter(temp_results));
        results = temp_results;
      }
    }
  }
  return results;
}

void WikiSearcher::PrintSearchResults(const vector<int>& results) const {
  if (results.empty()) {
    printf("Nothing found\n");
  } else {
    size_t max_index = min(results.size(), TOP_RESULTS_NUMBER_);
    for (size_t index = 0; index < max_index - 1; ++index) {
      printf("%s; ", document_names_[results[index] - 1].data());
    }
    printf("%s\n", document_names_[results[max_index - 1] - 1].data());
  }
}