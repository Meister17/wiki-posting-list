#ifndef ELIAS_OMEGA_DECOMPRESSOR_H
#define ELIAS_OMEGA_DECOMPRESSOR_H

#include "decompressor.h"
#include "../util/bitreader.h"
#include <vector>

class EliasOmegaDecompressor : public PostingListDecompressor {
 public:
  std::vector<int> DecompressPostingList(
      const std::vector<unsigned char>& bytes, int length) const {
    std::vector<int> integers;
    BitReader bitreader(bytes, length);
    int prev_number = 0;
    while (bitreader.HasLeft()) {
      int number = 1;
      while (bitreader.GetBit()) {
        int len = number;
        number = 1;
        for (int i = 0; i < len; ++i) {
          number <<= 1;
          if (bitreader.GetBit()) {
            number |= 1;
          }
        }
      }
      integers.push_back(number + prev_number);
      prev_number = integers.back();
    }
    return integers;
  }
};

#endif /* ELIAS_OMEGA_DECOMPRESSOR_H */