#ifndef ELIAS_GAMMA_DECOMPRESSOR_H
#define ELIAS_GAMMA_DECOMPRESSOR_H

#include "decompressor.h"
#include "../util/bitreader.h"
#include <vector>

class EliasGammaDecompressor : public PostingListDecompressor {
 public:
  std::vector<int> DecompressPostingList(
      const std::vector<unsigned char>& bytes, int length) const {
    std::vector<int> integers;
    BitReader bitreader(bytes, length);
    int prev_number = 0;
    while (bitreader.HasLeft()) {
      int number_bits = 0;
      while (!bitreader.GetBit() && bitreader.HasLeft()) {
        ++number_bits;
      }
      int current = 0;
      for (int a = number_bits - 1; a >= 0; --a) {
        unsigned char bit = bitreader.GetBit();
        if (bit) {
          current |= bit << a;
        }
      }
      current |= 1 << number_bits;
      integers.push_back(current + prev_number);
      prev_number = integers.back();
    }
    return integers;
  }
};

#endif /* ELIAS_GAMMA_DECOMPRESSOR_H */