#ifndef NO_POSTING_LIST_DECOMPRESSOR_H
#define NO_POSTING_LIST_DECOMPRESSOR_H

#include "decompressor.h"
#include "../util/bitreader.h"
#include <vector>

class NoPostingListDecompressor : public PostingListDecompressor {
 public:
  std::vector<int> DecompressPostingList(
      const std::vector<unsigned char>& bytes, int length) const {
    std::vector<int> integers;
    BitReader bitreader(bytes, length);
    int prev_number = 0;
    while (bitreader.HasLeft()) {
      int number = 0x0;
      for (int i = 0; i < 32 && bitreader.HasLeft(); ++i) {
        number = (number << 1) | bitreader.GetBit();
      }
      integers.push_back(number + prev_number);
      prev_number = integers.back();
    }
    return integers;
  }
};

#endif /* NO_POSTING_LIST_DECOMPRESSOR_H */