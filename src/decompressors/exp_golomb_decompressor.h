#ifndef EXP_GOLOMB_DECOMPRESSOR_H
#define EXP_GOLOMB_DECOMPRESSOR_H

#include "decompressor.h"
#include "../util/bitreader.h"
#include <vector>

class ExpGolombDecompressor : public PostingListDecompressor {
 public:
  std::vector<int> DecompressPostingList(
      const std::vector<unsigned char>& bytes, int length) const {
    std::vector<int> integers;
    BitReader bitreader(bytes, length);
    int prev_number = 0;
    while (bitreader.HasLeft()) {
      int number = 1;
      int l = 0;
      while (!bitreader.GetBit()) {
        ++l;
      }
      for (int i = 0; i < l; ++i) {
        number = (number << 1) | bitreader.GetBit();
      }
      integers.push_back(number + prev_number - 1);
      prev_number = integers.back();
    }
    return integers;
  }
};

#endif /* EXP_GOLOMB_DECOMPRESSOR_H */