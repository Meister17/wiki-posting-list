#ifndef DECOMPRESSOR_H
#define DECOMPRESSOR_H

#include <vector>

class PostingListDecompressor {
 public:
  virtual std::vector<int> DecompressPostingList(
      const std::vector<unsigned char>& bytes, int length) const = 0;

  virtual ~PostingListDecompressor() {
  }
};

#endif /* DECOMPRESSOR_H */