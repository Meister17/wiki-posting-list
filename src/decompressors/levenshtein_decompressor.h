#ifndef LEVENSHTEIN_DECOMPRESSOR_H
#define LEVENSHTEIN_DECOMPRESSOR_H

#include "decompressor.h"
#include "../util/bitreader.h"
#include <vector>

class LevenshteinDecompressor : public PostingListDecompressor {
 public:
  std::vector<int> DecompressPostingList(
      const std::vector<unsigned char>& bytes, int length) const {
    std::vector<int> integers;
    BitReader bitreader(bytes, length);
    int prev_number = 0;
    while (bitreader.HasLeft()) {
      int n = 0;
      while (bitreader.GetBit()) {
        ++n;
      }
      int number;
      if (n == 0) {
        number = 0;
      } else {
        number = 1;
        for (int i = 0; i < n - 1; ++i) {
          int val = 1;
          for (int j = 0; j < number; ++j) {
            val = (val << 1) | bitreader.GetBit();
          }
          number = val;
        }
      }
      integers.push_back(number + prev_number);
      prev_number = integers.back();
    }
    return integers;
  }
};

#endif /* LEVENSHTEIN_DECOMPRESSOR_H */