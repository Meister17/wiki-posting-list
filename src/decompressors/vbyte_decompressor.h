#ifndef VBYTE_DECOMPRESSOR_H
#define VBYTE_DECOMPRESSOR_H

#include "decompressor.h"
#include "../util/bitreader.h"
#include <vector>

class VByteDecompressor : public PostingListDecompressor {
 public:
  std::vector<int> DecompressPostingList(
      const std::vector<unsigned char>& bytes, int length) const {
    std::vector<int> integers;
    BitReader bitreader(bytes, length);
    int prev_number = 0;
    while (bitreader.HasLeft()) {
      int number = 0;
      do {
        number <<= 7;
        int mod = 0;
        for (int i = 0; i < 7; ++i) {
          mod = (mod << 1) | bitreader.GetBit();
        }
        number |= mod;
      } while (bitreader.GetBit());
      integers.push_back(number + prev_number);
      prev_number = integers.back();
    }
    return integers;
  }
};

#endif /* VBYTE_DECOMPRESSOR_H */