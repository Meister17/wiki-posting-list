#ifndef ELIAS_DELTA_COMPRESSOR_H
#define ELIAS_DELTA_COMPRESSOR_H

#include "decompressor.h"
#include "../util/bitreader.h"
#include <vector>

class EliasDeltaDecompressor : public PostingListDecompressor {
 public:
  std::vector<int> DecompressPostingList(
      const std::vector<unsigned char>& bytes, int length) const {
    std::vector<int> integers;
    BitReader bitreader(bytes, length);
    int prev_number = 0;
    while (bitreader.HasLeft()) {
      int number = 1;
      int len = 1;
      int lengthOfLen = 0;
      while (!bitreader.GetBit()) {
        ++lengthOfLen;
      }
      for (int i = 0; i < lengthOfLen; ++i) {
        len <<= 1;
        if (bitreader.GetBit()) {
          len |= 1;
        }
      }
      for (int i = 0; i < len - 1; ++i) {
        number <<= 1;
        if (bitreader.GetBit()) {
          number |= 1;
        }
      }
      integers.push_back(number + prev_number);
      prev_number = integers.back();
    }
    return integers;
  }
};

#endif /* ELIAS_DELTA_COMPRESSOR_H */