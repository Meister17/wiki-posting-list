#ifndef ELIAS_OMEGA_COMPRESSOR_H
#define ELIAS_OMEGA_COMPRESSOR_H

#include "compressor.h"
#include "../util/bitwriter.h"
#include <stack>
#include <vector>

class EliasOmegaCompressor : public PostingListCompressor {
 public:
  std::vector<unsigned char> CompressPostingList(
      const std::vector<int>& posting_list) const {
    BitWriter bitwriter;
    int prev_number = 0;
    for (auto& doc_id: posting_list) {
      int number = doc_id - prev_number;
      std::stack<unsigned char> bits;
      while (number > 1) {
        int len = 0;
        for (int temp = number; temp > 0; temp >>= 1) {
          ++len;
        }
        for (int i = 0; i < len; ++i) {
          bits.push((number >> i) & 1);
        }
        number = len - 1;
      }
      while (!bits.empty()) {
        bitwriter.PutBit(bits.top());
        bits.pop();
      }
      bitwriter.PutBit(0);
      prev_number = doc_id;
    }
    return bitwriter.ToArray();
  }
};

#endif /* ELIAS_OMEGA_COMPRESSOR_H */