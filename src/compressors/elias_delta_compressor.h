#ifndef ELIAS_DELTA_COMPRESSOR_H
#define ELIAS_DELTA_COMPRESSOR_H

#include "compressor.h"
#include "../util/bitwriter.h"
#include <vector>

class EliasDeltaCompressor : public PostingListCompressor {
 public:
  std::vector<unsigned char> CompressPostingList(
      const std::vector<int>& posting_list) const {
    BitWriter bitwriter;
    int prev_number = 0;
    for (auto& doc_id: posting_list) {
      int number = doc_id - prev_number;
      int len = 0;
      int lengthOfLen = 0;
      for (int temp = number; temp > 0; temp >>= 1) {
        ++len;
      }
      for (int temp = len; temp > 1; temp >>= 1) {
        ++lengthOfLen;
      }
      for (int i = lengthOfLen; i > 0; --i) {
        bitwriter.PutBit(0x0);
      }
      for (int i = lengthOfLen; i >= 0; --i) {
        bitwriter.PutBit((len >> i) & 1);
      }
      for (int i = len - 2; i >= 0; --i) {
        bitwriter.PutBit((number >> i) & 1);
      }
      prev_number = doc_id;
    }
    return bitwriter.ToArray();
  }
};

#endif /* ELIAS_DELTA_COMPRESSOR_H */