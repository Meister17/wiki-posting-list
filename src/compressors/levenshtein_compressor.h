#ifndef LEVENSHTEIN_COMPRESSOR_H
#define LEVENSHTEIN_COMPRESSOR_H

#include "compressor.h"
#include "../util/bitwriter.h"
#include <stack>
#include <vector>

class LevenshteinCompressor : public PostingListCompressor {
 public:
  std::vector<unsigned char> CompressPostingList(
      const std::vector<int>& posting_list) const {
    BitWriter bitwriter;
    int prev_number = 0;
    for (auto& doc_id: posting_list) {
      int number = doc_id - prev_number;
      if (number == 0) {
        bitwriter.PutBit(0);
      } else {
        int c = 0;
        std::stack<unsigned char> bits;
        do {
          int m = 0;
          for (int temp = number; temp > 1; temp >>= 1) {
            ++m;
          }
          for (int i = 0; i < m; ++i) {
            bits.push((number >> i) & 1);
          }
          number = m;
          ++c;
        } while (number > 0);
        for (int i = 0; i < c; ++i) {
          bitwriter.PutBit(1);
        }
        bitwriter.PutBit(0);
        while (!bits.empty()) {
          bitwriter.PutBit(bits.top());
          bits.pop();
        }
      }
      prev_number = doc_id;
    }
    return bitwriter.ToArray();
  }
};

#endif /* LEVENSHTEIN_COMPRESSOR_H */