#ifndef VBYTE_COMPRESSOR_H
#define VBYTE_COMPRESSOR_H

#include "compressor.h"
#include "../util/bitwriter.h"
#include <vector>

class VByteCompressor : public PostingListCompressor {
 public:
  std::vector<unsigned char> CompressPostingList(
      const std::vector<int>& posting_list) const {
    BitWriter bitwriter;
    int prev_number = 0;
    for (auto& doc_id: posting_list) {
      EncodeNumber(doc_id - prev_number, &bitwriter);
      bitwriter.PutBit(0);
      prev_number = doc_id;
    }
    return bitwriter.ToArray();
  }

 private:
  void EncodeNumber(int number, BitWriter* bitwriter) const {
    if (number >= 128) {
      EncodeNumber(number >> 7, bitwriter);
      bitwriter->PutBit(1);
    }
    for (int i = 6; i >= 0; --i) {
      bitwriter->PutBit((number >> i) & 1);
    }
  }
};

#endif /* VBYTE_COMPRESSOR_H */