#ifndef EXP_GOLOMB_COMPRESSOR_H
#define EXP_GOLOMB_COMPRESSOR_H

#include "compressor.h"
#include "../util/bitwriter.h"
#include <vector>

class ExpGolombCompressor : public PostingListCompressor {
 public:
  std::vector<unsigned char> CompressPostingList(
      const std::vector<int>& posting_list) const {
    BitWriter bitwriter;
    int prev_number = 0;
    for (auto& doc_id: posting_list) {
      int number = doc_id - prev_number + 1;
      int l = 0;
      for (int temp = number; temp > 1; temp >>= 1) {
        ++l;
      }
      for (int i = 0; i < l; ++i) {
        bitwriter.PutBit(0);
      }
      for (int i = l; i >=0; --i) {
        bitwriter.PutBit((number >> i) & 1);
      }
      prev_number = doc_id;
    }
    return bitwriter.ToArray();
  }
};

#endif /* EXP_GOLOMB_COMPRESSOR_H */