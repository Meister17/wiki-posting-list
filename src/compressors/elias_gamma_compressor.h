#ifndef ELIAS_GAMMA_COMPRESSOR_H
#define ELIAS_GAMMA_COMPRESSOR_H

#include "compressor.h"
#include "../util/bitwriter.h"
#include <vector>

class EliasGammaCompressor : public PostingListCompressor {
 public:
  std::vector<unsigned char> CompressPostingList(
      const std::vector<int>& posting_list) const {
    BitWriter bitwriter;
    int prev_number = 0;
    for (auto& doc_id: posting_list) {
      int number = doc_id - prev_number;
      int l = 0;
      for (int temp = number; temp > 1; temp >>= 1) {
        ++l;
      }
      for (int a = 0; a < l; ++a) {
        bitwriter.PutBit(0);
      }
      bitwriter.PutBit(1);
      for (int a = l - 1; a >= 0; --a) {
        if (number & 1 << a) {
          bitwriter.PutBit(1);
        } else {
          bitwriter.PutBit(0);
        }
      }
      prev_number = doc_id;
    }
    return bitwriter.ToArray();
  }
};

#endif /* ELIAS_GAMMA_COMPRESSOR_H */