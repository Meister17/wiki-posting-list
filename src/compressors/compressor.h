#ifndef COMPRESSOR_H
#define COMPRESSOR_H

#include <vector>

class PostingListCompressor {
 public:
  virtual std::vector<unsigned char> CompressPostingList(
      const std::vector<int>& posting_list) const = 0;

  virtual ~PostingListCompressor() {
  }
};

#endif /* COMPRESSOR_H */