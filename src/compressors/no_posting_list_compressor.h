#ifndef NO_POSTING_LIST_COMPRESSOR_H
#define NO_POSTING_LIST_COMPRESSOR_H

#include "compressor.h"
#include "../util/bitwriter.h"
#include <vector>

class NoPostingListCompressor : public PostingListCompressor {
 public:
  std::vector<unsigned char> CompressPostingList(
      const std::vector<int>& posting_list) const {
    BitWriter bitwriter;
    int prev_number = 0;
    for (auto& doc_id: posting_list) {
      bitwriter.InsertNumber(doc_id - prev_number);
      prev_number = doc_id;
    }
    return bitwriter.ToArray();
  }
};

#endif /* NO_POSTING_LIST_COMPRESSOR_H */