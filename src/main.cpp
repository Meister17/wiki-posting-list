#include "program_options_parser.h"
#include "posting_list_creator.h"
#include "wiki_searcher.h"
#include <cstdio>
#include <stdexcept>

using std::exception;
using std::runtime_error;

int main(int argc, char** argv) {
  try {
    ProgramOptionsParser program_options_parser;
    program_options_parser.Parse(argc, argv);
    if (program_options_parser.mode() == "create_index") {
      PostingListCreator posting_list_creator;
      posting_list_creator.DoJob(
          program_options_parser.input_directory_name(),
          program_options_parser.vocabulary_filename(),
          program_options_parser.document_names_filename(),
          program_options_parser.posting_list_filename(),
          program_options_parser.compression_method(),
          program_options_parser.exact_duplicates_filename(),
          program_options_parser.near_duplicates_filename(),
          program_options_parser.exact_duplicates_algorithm(),
          program_options_parser.near_duplicates_algorithm());
    } else if (program_options_parser.mode() == "search") {
      WikiSearcher wiki_searcher(program_options_parser.top_results_number());
      wiki_searcher.Initialize(program_options_parser.posting_list_filename(),
                               program_options_parser.vocabulary_filename(),
                               program_options_parser.document_names_filename(),
                               program_options_parser.compression_method());
      wiki_searcher.SearchWiki();
    } else {
      throw runtime_error("Wrong program mode");
    }
    return 0;
  } catch (exception& error) {
    printf("Fatal error occurred: %s\n", error.what());
    return 1;
  }
}